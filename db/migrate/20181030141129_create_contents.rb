class CreateContents < ActiveRecord::Migration[5.1]
  def change
    create_table :contents do |t|
      t.integer :studio_num
      t.integer :day_num
      t.integer :time_num
      t.string :studio_text
      t.string :day_text
      t.string :time_text
      t.string :instructor
      t.string :content_text
      t.string :content_url

      t.timestamps
    end
  end
end
