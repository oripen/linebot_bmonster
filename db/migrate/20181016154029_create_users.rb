class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :line_id
      t.string :bmon_id
      t.string :bmon_pass

      t.timestamps
    end
  end
end
