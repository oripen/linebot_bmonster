# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181030141129) do

  create_table "contents", force: :cascade do |t|
    t.integer "studio_num"
    t.integer "day_num"
    t.integer "time_num"
    t.string "studio_text"
    t.string "day_text"
    t.string "time_text"
    t.string "instructor"
    t.string "content_text"
    t.string "content_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "days", force: :cascade do |t|
    t.integer "number"
    t.string "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "line_id"
    t.string "bmon_id"
    t.string "bmon_pass"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
