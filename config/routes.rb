Rails.application.routes.draw do
  get 'pushbot/home'
  post '/callback' => 'linebot#callback'
  get  'linebot/home'
  root 'linebot#home'
  
end