class LinebotController < ApplicationController
  require 'line/bot'
  require 'date'
  require 'nokogiri'
  require 'open-uri'
  require "sanitize"
  require 'date'
  require 'selenium-webdriver'
  require 'phantomjs'

  def home
  end

  # callbackアクションのCSRFトークン認証を無効
  protect_from_forgery :except => [:callback]

  def client
    @client ||= Line::Bot::Client.new { |config|
      config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
    }
  end

  def callback
    body = request.body.read

    signature = request.env['HTTP_X_LINE_SIGNATURE']
    unless client.validate_signature(body, signature)
      error 400 do 'Bad Request' end
    end

    events = client.parse_events_from(body)
    events.each { |event|
      case event
      when Line::Bot::Event::Message
        case event.type
        when Line::Bot::Event::MessageType::Text
          args = event.message['text'].split(" ")
          
          if args[0] == "スケジュール"
            if args.length == 1
              client.reply_message(event['replyToken'], getSchedule1)
            end
            if args.length == 2
              client.reply_message(event['replyToken'], getSchedule2(args[1]))
            end
          end
          
          if args[0] == "予約"
            if args.length == 1
              client.reply_message(event['replyToken'], getReserve1)
            end
            if args.length == 2
              client.reply_message(event['replyToken'], getReserve2(args[1]))
            end
            if args.length == 3
              client.reply_message(event['replyToken'], getReserve3(args[1], args[2]))
            end
            if args.length == 4
              userId = JSON.parse(body, quirks_mode: true)['events'][0]['source']['userId']
              puts "userId: " + userId
              inoFlg = false
              inoFlg = true if userId == "U346998bc6c77fa1971425543fdcf14af"
              client.reply_message(event['replyToken'], getReserve4(args[1], args[2], args[3], inoFlg))
            end
          end
          
          if args[0] == "登録"
            if args.length == 3
              userId = JSON.parse(body, quirks_mode: true)['events'][0]['source']['userId']
              client.reply_message(event['replyToken'], saveUser(userId, args[1], args[2]))
            end
          end
          
          if args[0] == "確認"
            if args.length == 1
              userId = JSON.parse(body, quirks_mode: true)['events'][0]['source']['userId']
              client.reply_message(event['replyToken'], confirmUser(userId))
            end
          end
          
          if args[0] == "キャンセル"
            if args.length == 1
              client.reply_message(event['replyToken'], doCancel)
            end
          end
          
          if args[0] == "更新"
            if args.length == 1
              client.push_message("U346998bc6c77fa1971425543fdcf14af", updateBaseInfo)
            end
          end
          
        end
      end
    }

    head :ok
  end
  
  def makeDays
    days = Array.new
    baseDay = Time.now.in_time_zone('Tokyo').ago(7.hours)
    for i in 0..6
      day = baseDay.since(i.days)
      days << day.strftime("%m月%d日(") + %w(日 月 火 水 木 金 土)[day.wday] + ")"
    end
    return days
  end
  
  def makeDayNo(dayInfo)
    dayNo = 99
    days = makeDays
    for i in 0..6
      dayNo = i if dayInfo == days[i]
    end
    return dayNo
  end
  
  def makeTimeNo(timeInfo)
    timeList = Array.new(["07:00", "08:05", "09:20", "10:35", "11:50", "13:05", "14:20", "15:35", "16:50", "18:05", "19:20", "20:35", "21:50"])
    timeNo = 99
    for i in 0..12
      timeNo = i if timeInfo == timeList[i]
    end
    return timeNo
  end
  
  def makeBagList(areaInfo)
    areaList = Array.new(["オススメエリア", "右上エリア", "左上エリア", "右下エリア", "左下エリア"])

    bagCenter = Array.new([15,16,17,18,19,20,21,22,23,24,29,30,31,32,33,34,35,36,37,38,43,44,45,46,47,48,49,50,54,55,56,57,58,59,60,61,62,63])
    bagMigiUe = Array.new([1,2,3,4,5,6,7,8,9,10])
    bagHidariUe = Array.new([68,69,70,71,72,73,74,75,76,77,82,83,84,85,86,87,88,89,90,91])
    bagMigiShita = Array.new([11,12,13,14,25,26,27,28,39,40,41,42])
    bagHidariShita = Array.new([51,52,53,64,65,66,67,78,79,80,81,92,93,94,95])
    
    bagList = Array.new
    bagList = bagCenter if areaInfo == areaList[0]
    bagList = bagMigiUe if areaInfo == areaList[1]
    bagList = bagHidariUe if areaInfo == areaList[2]
    bagList = bagMigiShita if areaInfo == areaList[3]
    bagList = bagHidariShita if areaInfo == areaList[4]
    
    return bagList
  end
  
  def makeReserveTimeMessage(dayInfo, replyMessage)
    timeList = Array.new(["07:00", "08:05", "09:20", "10:35", "11:50", "13:05", "14:20", "15:35", "16:50", "18:05", "19:20", "20:35", "21:50"])
    message =
      {type: 'text', text: replyMessage},
      { type: "text", 
        text: "予約したい時間の選択\n※メニュー閉じて↑にスワイプ", 
        "quickReply": {
          "items": [
            {"type": "action", "action": { "type": "message", "label": timeList[0], "text": "予約 " + dayInfo + " " + timeList[0] } },
            {"type": "action", "action": { "type": "message", "label": timeList[1], "text": "予約 " + dayInfo + " " + timeList[1] } },
            {"type": "action", "action": { "type": "message", "label": timeList[2], "text": "予約 " + dayInfo + " " + timeList[2] } },
            {"type": "action", "action": { "type": "message", "label": timeList[3], "text": "予約 " + dayInfo + " " + timeList[3] } },
            {"type": "action", "action": { "type": "message", "label": timeList[4], "text": "予約 " + dayInfo + " " + timeList[4] } },
            {"type": "action", "action": { "type": "message", "label": timeList[5], "text": "予約 " + dayInfo + " " + timeList[5] } },
            {"type": "action", "action": { "type": "message", "label": timeList[6], "text": "予約 " + dayInfo + " " + timeList[6] } },
            {"type": "action", "action": { "type": "message", "label": timeList[7], "text": "予約 " + dayInfo + " " + timeList[7] } },
            {"type": "action", "action": { "type": "message", "label": timeList[8], "text": "予約 " + dayInfo + " " + timeList[8] } },
            {"type": "action", "action": { "type": "message", "label": timeList[9], "text": "予約 " + dayInfo + " " + timeList[9] } },
            {"type": "action", "action": { "type": "message", "label": timeList[10], "text": "予約 " + dayInfo + " " + timeList[10] } },
            {"type": "action", "action": { "type": "message", "label": timeList[11], "text": "予約 " + dayInfo + " " + timeList[11] } },
            {"type": "action", "action": { "type": "message", "label": timeList[12], "text": "予約 " + dayInfo + " " + timeList[12] } }
          ]
        }
      }
    return message
  end
  
  def getSchedule1
    studios = Array.new(["GINZA", "AOYAMA", "EBISU", "SHINJUKU", "SAKAE", "IKEBUKURO"])
    message = {
      type: "text", 
      text: "確認したいスタジオの選択\n※メニュー閉じて↑にスワイプ", 
      "quickReply": {
        "items": [
          { "type": "action", "action": { "type": "message", "label": studios[0], "text": "スケジュール " + studios[0] } },
          { "type": "action", "action": { "type": "message", "label": studios[1], "text": "スケジュール " + studios[1] } },
          { "type": "action", "action": { "type": "message", "label": studios[2], "text": "スケジュール " + studios[2] } },
          { "type": "action", "action": { "type": "message", "label": studios[3], "text": "スケジュール " + studios[3] } },
          { "type": "action", "action": { "type": "message", "label": studios[4], "text": "スケジュール " + studios[4] } },
          { "type": "action", "action": { "type": "message", "label": studios[4], "text": "スケジュール " + studios[5] } }
        ]
      }
    }
    return message
  end
  
  def getSchedule2(studioInfo)
    studios = Array.new(["GINZA", "AOYAMA", "EBISU", "SHINJUKU", "SAKAE", "IKEBUKURO"])
    studioUrls = Array.new([
      "https://www.b-monster.jp/reserve/?studio_code=0001",
      "https://www.b-monster.jp/reserve/?studio_code=0002",
      "https://www.b-monster.jp/reserve/?studio_code=0003",
      "https://www.b-monster.jp/reserve/?studio_code=0004",
      "https://www.b-monster.jp/reserve/?studio_code=0005",
      "https://www.b-monster.jp/reserve/?studio_code=0006"
    ])
    
    studioVali = false
    studioNo = 99
    for i in 0..5
      if studioInfo == studios[i]
        studioVali = true
        studioNo = i
      end
    end
    if studioVali == false
      return message = {type: 'text', text: 'スタジオ情報が正しくないです'}
    end
    
    doc = Nokogiri::HTML(open(studioUrls[studioNo]))

    scheduleList = Array.new
    for itr_day in 0..6
      baseHTML = doc.css(".flex-no-wrap")[itr_day]
      day = baseHTML.css(".column-header h3").text
      replyMessage = day + "\n"
      for timeNum in 0..12
        content = baseHTML.css(".panel-content")[itr_prg]
        mode = content.css(".tt-mode")[0]["data-program"]
        time = content.css(".tt-time").text[0, 5]
        instructor = content.css(".tt-instructor").text
        instructor = "※無料体験会" if mode == "無料体験会"
        instructor = "※STREAM ONLY" if mode == "STREAM ONLY" 
        replyMessage = replyMessage + "\n" + time + " - " + instructor
      end
      scheduleList << replyMessage
    end
    
    message = 
      {type: 'text', text: scheduleList[0]},
      {type: 'text', text: scheduleList[1]},
      {type: 'text', text: scheduleList[2]}
    
    return message
  end
  
  def getReserve1
    days = makeDays
    message = {
      type: "text", 
      text: "予約したい日付の選択\n※メニュー閉じて↑にスワイプ", 
      "quickReply": {
        "items": [
          { "type": "action", "action": { "type": "message", "label": days[0], "text": "予約 " + days[0] } },
          { "type": "action", "action": { "type": "message", "label": days[1], "text": "予約 " + days[1] } },
          { "type": "action", "action": { "type": "message", "label": days[2], "text": "予約 " + days[2] } },
          { "type": "action", "action": { "type": "message", "label": days[3], "text": "予約 " + days[3] } },
          { "type": "action", "action": { "type": "message", "label": days[4], "text": "予約 " + days[4] } },
          { "type": "action", "action": { "type": "message", "label": days[5], "text": "予約 " + days[5] } },
          { "type": "action", "action": { "type": "message", "label": days[6], "text": "予約 " + days[6] } }
        ]
      }
    }
    return message
  end

  def getReserve2(dayInfo)
    dayNo = makeDayNo(dayInfo)
    if dayNo == 99
      return {type: 'text', text: '日付情報が正しくないです'}
    end
    
    ebisu = "https://www.b-monster.jp/reserve/?studio_code=0003"
    doc = Nokogiri::HTML(open(ebisu))
    baseHTML = doc.css(".flex-no-wrap")[dayNo]
    day = baseHTML.css(".column-header h3").text
    replyMessage = day + "\n"
    for itr_prg in 0..12
      content = baseHTML.css(".panel-content")[itr_prg]
      mode = content.css(".tt-mode")[0]["data-program"]
      time = content.css(".tt-time").text[0, 5]
      instructor = content.css(".tt-instructor").text
      instructor = "※無料体験会" if mode == "無料体験会"
      instructor = "※STREAM ONLY" if mode == "STREAM ONLY" 
      replyMessage = replyMessage + "\n" + time + " - " + instructor
    end
    replyMessage = replyMessage + "\n\n※は予約不可" 
    
    return makeReserveTimeMessage(dayInfo, replyMessage)
  end

  def getReserve3(dayInfo, timeInfo)
    bagList = Array.new(["オススメエリア", "右上エリア", "左上エリア", "右下エリア", "左下エリア"])
    
    dayNo = makeDayNo(dayInfo)
    if dayNo == 99
      return {type: 'text', text: '日付情報が正しくないです'}
    end
    timeNo = makeTimeNo(timeInfo)
    if timeNo == 99
      return {type: 'text', text: '時間情報が正しくないです'}
    end
    
    ebisu = "https://www.b-monster.jp/reserve/?studio_code=0003"
    doc = Nokogiri::HTML(open(ebisu))
    
    baseHTML = doc.css(".flex-no-wrap")[dayNo]
    url = baseHTML.css("li")[timeNo].css("a")[0][:href]
    
    doc = Nokogiri::HTML(open(url))
    if doc.css("#bag1").empty?
      replyMessage = '満員なので予約できません'
      return makeReserveTimeMessage(dayInfo, replyMessage)
    end
    
    st = Array.nerw
    for i in 1..95
      bagId = "#bag" + i.to_s
      status = doc.css(bagId)[0]["disabled"] == "disabled" ? i.to_s : 'x'
      st << status + "\n"
    end

    wait = Selenium::WebDriver::Wait.new(:timeout => 100)
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options
    driver.navigate.to url
    driver.find_element(:class, 'modal-close').click
    wait.until {driver.find_element(:class, 'form-action').size = 0}
    driver.find_element(:id, 'ebisu-stage').location_once_scrolled_into_view
    driver.manage.window.resize_to(800, 900)
    driver.save_screenshot './public/bag.png'

    message =
      {
      "type": "image",
      "originalContentUrl": "https://glacial-scrubland-66337.herokuapp.com/bag.png",
      "previewImageUrl": "https://glacial-scrubland-66337.herokuapp.com/bag.png"
      },
      { type: "text", 
        text: "予約したいバッグの選択\n※メニュー閉じて↑にスワイプ", 
        "quickReply": {
          "items": [
            {"type": "action", "action": { "type": "message", "label": bagList[0], "text": "予約 " + dayInfo + " " + timeInfo + " " + bagList[0] } },
            {"type": "action", "action": { "type": "message", "label": bagList[1], "text": "予約 " + dayInfo + " " + timeInfo + " " + bagList[1] } },
            {"type": "action", "action": { "type": "message", "label": bagList[2], "text": "予約 " + dayInfo + " " + timeInfo + " " + bagList[2] } },
            {"type": "action", "action": { "type": "message", "label": bagList[3], "text": "予約 " + dayInfo + " " + timeInfo + " " + bagList[3] } },
            {"type": "action", "action": { "type": "message", "label": bagList[4], "text": "予約 " + dayInfo + " " + timeInfo + " " + bagList[4] } }
          ]
        }
      }

    return message
  end

  def getReserve4(dayInfo, timeInfo, areaInfo, inoFlg)
    if inoFlg == false
      return {type: 'text', text: "※テストユーザー以外予約不可"}
    end
    
    dayNo = makeDayNo(dayInfo)
    if dayNo == 99
      return {type: 'text', text: '日付情報が正しくないです'}
    end
    timeNo = makeTimeNo(timeInfo)
    if timeNo == 99
      return {type: 'text', text: '時間情報が正しくないです'}
    end
    bagList = makeBagList(areaInfo)
    if bagList.empty?
      return {type: 'text', text: 'バッグ情報が正しくないです'}
    end
    
    ebisu = "https://www.b-monster.jp/reserve/?studio_code=0003"
    doc = Nokogiri::HTML(open(ebisu))
    
    baseHTML = doc.css(".flex-no-wrap")[dayNo]
    url = baseHTML.css("li")[timeNo].css("a")[0][:href]
    
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options
    driver.navigate.to url
    
    element = driver.find_element(:name, 'login-username')
    element.send_keys('kaori334@icloud.com')
    element = driver.find_element(:name, 'login-password')
    element.send_keys('opc41748')
    driver.find_element(:id, 'login-btn').click
    
    doc = Nokogiri::HTML(open(url))
    if doc.css("#bag1").empty?
      replyMessage = '満員なので予約できません'
      return makeReserveTimeMessage(dayInfo, replyMessage)
    end
    
    message = {type: 'text', text: '予約できなかった'}
    bagList.each { |e|
      bagId = "#bag" + e.to_s
      bagClass = "bag" + e.to_s
      bagNo = e.to_s
      bagJudge = doc.css(bagId)[0]["disabled"]
      if bagJudge != "disabled"
        driver.find_element(:class, bagClass).find_element(:tag_name, 'span').click
        driver.find_element(:class, 'btn-gray').click
        wait.until {driver.find_element(:class, 'form-action').displayed?}
        driver.find_element(:class, 'form-action').find_element(:class, 'btn-orange').click
        message = {type: 'text', text: 'バッグNo「' + bagNo + '」で予約しました'}
        break
      end
      message = {type: 'text', text: '空きがなかった'}
    }
    
    return message
  end

  def doCancel
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options
    driver.navigate.to url
    
    element = driver.find_element(:name, 'login-username')
    element.send_keys('oripen301@gmail.com')
    element = driver.find_element(:name, 'login-password')
    element.send_keys('kameusa3848')
    driver.find_element(:id, 'login-btn').click
  end

  def saveUser(line_id, bmon_id, bmon_pass)
    user = User.find_by_line_id(line_id)
    if user.nil?
      user = User.new(line_id: line_id, bmon_id: bmon_id, bmon_pass: bmon_pass)
    else
      user.bmon_id = bmon_id
      user.bmon_pass = bmon_pass
    end
    user.save
    return {type: 'text', text: "登録したよ"}
  end
  
  def confirmUser(line_id)
    user = User.find_by_line_id(line_id)
    message = "id: " + user.bmon_id + "\n" + "pass: " + user.bmon_pass
    return  {type: 'text', text: message}
  end
  
  def updateBaseInfo
    
    days = makeDays
    for i in 0..6
      day = Day.where(["number = ?", i]).first
      if day.nil?
        day = Day.new(number: i, text: days[i])
      else
        day.text = days[i]
      end
      day.save
    end
    
    studios = Array.new(["GINZA", "AOYAMA", "EBISU", "SHINJUKU", "SAKAE", "IKEBUKURO"])
    studioUrls = Array.new([
      "https://www.b-monster.jp/reserve/?studio_code=0001",
      "https://www.b-monster.jp/reserve/?studio_code=0002",
      "https://www.b-monster.jp/reserve/?studio_code=0003",
      "https://www.b-monster.jp/reserve/?studio_code=0004",
      "https://www.b-monster.jp/reserve/?studio_code=0005",
      "https://www.b-monster.jp/reserve/?studio_code=0006"
    ])
    timeList = Array.new(["07:00", "08:05", "09:20", "10:35", "11:50", "13:05", "14:20", "15:35", "16:50", "18:05", "19:20", "20:35", "21:50"])
    for studioNum in 0..5
      doc = Nokogiri::HTML(open(studioUrls[studioNum]))
      for dayNum in 0..6
        baseHTML = doc.css(".flex-no-wrap")[dayNum]
        day = baseHTML.css(".column-header h3").text
        contentText = day + "\n"
        for timeNum in 0..12
          content = baseHTML.css(".panel-content")[timeNum]
          mode = content.css(".tt-mode")[0]["data-program"]
          time = content.css(".tt-time").text[0, 5]
          instructor = content.css(".tt-instructor").text
          instructor = "※無料体験会" if mode == "無料体験会"
          instructor = "※STREAM ONLY" if mode == "STREAM ONLY" 
          contentText = contentText + "\n" + time + " - " + instructor
          url = baseHTML.css("li")[timeNum].css("a")[0][:href]
          
          content = Content.where(["studio_num = ?", studioNum]).where(["day_num = ?", dayNum]).where(["time_num = ?", timeNum]).first
          if content.nil?
            content = Content.new(studio_num: studioNum,
                                  day_num: dayNum,
                                  time_num: timeNum,
                                  studio_text: studios[studioNum],
                                  day_text: days[dayNum],
                                  time_text: timeList[timeNum],
                                  instructor: instructor,
                                  content_text: contentText,
                                  content_url: url)
          else
            content.studio_text = studios[studioNum]
            content.day_text = days[dayNum]
            content.time_text = timeList[timeNum]
            content.instructor = instructor
            content.content_text = contentText
            content. content_url = url
          end
          content.save
          
        end
      end
    end
    
    return {type: 'text', text: "更新したよ"}
  end
  
  def test
    ebisu = "https://www.b-monster.jp/reserve/?studio_code=0003"
    doc = Nokogiri::HTML(open(ebisu))
    
    baseHTML = doc.css(".flex-no-wrap")[3]
    url = baseHTML.css("li")[12].css("a")[0][:href]
    
    #doc = Nokogiri::HTML(open(url))
    
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options
    driver.navigate.to url
    
    driver.find_element(:class, 'modal-close').click
    driver.find_element(:id, 'ebisu-stage').location_once_scrolled_into_view
    driver.manage.window.resize_to(800, 900)
    driver.save_screenshot './public/bag.png'
  end
  
  def test2
    #json = JSON.parse('{"events":[{"type":"message","replyToken":"3978f94670ec464b896b7d1688215d6d","source":{"userId":"U346998bc6c77fa1971425543fdcf14af","type":"user"},"timestamp":1539440970622,"message":{"type":"text","id":"8712766888364","text":"予約"}}]}', quirks_mode: true)
    #puts json['events'][0]['source']['userId']
    
    bagCenter = Array.new
    bagCenter << (15..24).to_a << (29..38).to_a << (43..50).to_a << (54..63).to_a
    bagMigiUe = Array.new
    bagMigiUe << (1..10).to_a
    bagHidariUe = Array.new
    bagHidariUe << (68..77).to_a << (82..91).to_a
    bagMigiShita = Array.new
    bagMigiShita << (11..14).to_a << (25..28).to_a << (39..42).to_a
    bagHidariShita = Array.new
    bagHidariShita << (51..53).to_a << (64..67).to_a << (78..81).to_a << (92..95).to_a
    
    puts bagCenter.join(",")
    puts bagMigiUe.join(",")
    puts bagHidariUe.join(",")
    puts bagMigiShita.join(",")
    puts bagHidariShita.join(",")
    
    bagHidariShita = Array.new([51,52,53,64,65,66,67,78,79,80,81,92,93,94,95])
    
    bagHidariShita.each { |e|
      bagId = "#bag" + e.to_s
      puts bagId
    }
  end

  def test3
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options
    driver.navigate.to "https://www.b-monster.jp/reserve/?studio_code=0003"
    driver.save_screenshot './app/assets/images/bmonbmonbmon.png'
  end

  def test4
    ebisu = "https://www.b-monster.jp/reserve/?studio_code=0003"
    doc = Nokogiri::HTML(open(ebisu))
    
    baseHTML = doc.css(".flex-no-wrap")[1]
    url = baseHTML.css("li")[12].css("a")[0][:href]
    
    doc = Nokogiri::HTML(open(url))
    if doc.css("#bag1").empty?
      replyMessage = '満員なので予約できません'
      return makeReserveTimeMessage(dayInfo, replyMessage)
    end
    
    st = Array.new
    for i in 1..95
      bagId = "#bag" + i.to_s
      status = doc.css(bagId)[0]["disabled"] == "disabled" ? 'x' : i.to_s
      st << status + "\n"
    end
    
    return st
  end
  
  def test5
    #wait = Selenium::WebDriver::Wait.new(:timeout => 3)
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options
    driver.navigate.to "https://www.b-monster.jp/reserve/punchbag?lesson_id=49525&studio_code=0003"
    driver.find_element(:class, 'modal-close').click
    #wait.until {driver.find_element(:class, 'modal-close')}
    aaa = driver.find_element(:css,'.modal-close').size() > 0
    driver.find_element(:id, 'ebisu-stage').location_once_scrolled_into_view
    driver.manage.window.resize_to(800, 900)
    driver.save_screenshot './public/bag.png'
  end
  
  def testDB
    user = User.new
    user.line_id = "aaa"
    user.bmon_id = "bbb"
    user.bmon_pass = "ccc"
    user.save
    puts User.find(2).line_id
  end
end
