=begin
require 'watir'

class UpdateChecker
  def initialize(url, interval = 180)
    @browser = Watir::Browser.start(url, :phantomjs)
    @interval = interval
    @old = nil
  end

  def start(&block)
    Thread.new do
      loop do
        s_time = Time.now
        check(&block)
        sleep [0, @interval - (Time.now - s_time)].max
      end
    end
  end

  def check
    new = nil
    @browser.refresh
    if block_given?
      new = yield(@browser)
    else
      new = @browser.html
    end

    unless @old.nil?
      if @old == new
        not_updated(@old)
      else
        updated(@old, new)
      end
    end
    @old = new
  end

  def updated(old, new)
    puts "#{@browser.title} updated on #{Time.now}"
    puts new
  end

  def not_updated(old)
    puts "#{@browser.title} not updated"
  end
end


checker = UpdateChecker.new("http://www.yahoo.co.jp/", 5)

def checker.updated(old, new)
  puts "latest news!"
  puts new
end

thread = checker.start do |browser|
  browser.div(id: "topicsboxbd").ul(class: "emphasis").text
end

thread.join
=end