class Day < ApplicationRecord
  validates :number, uniqueness: true
end